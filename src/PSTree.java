
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author chen
 */
public class PSTree {
    private static String getPidName(int pid){ // takes process id and returns its name
        File procFile = new File("/proc/"+pid + "/status");
        String statNameLine;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(procFile));
            statNameLine = reader.readLine();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PSTree.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(PSTree.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        Matcher m = Pattern.compile("Name:\\t([^$]+)").matcher(statNameLine);
        m.find();
        return m.group(1);
    }
    
    private static List<Integer> getChildTasks(int pid){ 
// takes a process id and returns its childs (process + threads)
        LinkedList<Integer> childTasks = new LinkedList<>();
        File procFolder = new File("/proc/" + pid + "/task");
        for (File listFile : procFolder.listFiles()) {
            childTasks.add(Integer.valueOf(listFile.getName()));
        }
        return childTasks;
    }
    
    private static void printRecursive(int pid,String tabs,HashSet<Integer> printedBefore){
        // prints recursvily process name + id then childs
        String myName = getPidName(pid);
        System.out.println(tabs + myName + " (" + pid + ")");
        printedBefore.add(pid);
        List<Integer> childTasks = getChildTasks(pid);
        for (Integer childTask : childTasks) {
            if (printedBefore.contains(childTask)) {
                continue;
            }
            printRecursive(childTask, tabs + "\t",printedBefore);
        }
    }
    
    private static void printRecursive(int pid){
        printRecursive(pid, "", new HashSet<>());
    }
    
    public static void main(String[] args) {
        int rootPid = 1;
        if (args.length==1 && args[0].matches("\\d+")) {
            rootPid = Integer.valueOf(args[0]);
        } else if (args.length!=0) {
            System.out.println("Bad args - pstree (pid)");
            return;
        }
        
        printRecursive(rootPid);
    }
    
    
}
